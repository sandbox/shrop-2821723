Security examples

This module demonstrates examples of development related security issues. Comments in the code discuss corrective actions.

Do not run this in production or on sites accessible to the public Internet. It is recommended to use this with a local development environment.

